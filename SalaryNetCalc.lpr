program SalaryNetCalc;

{$mode objfpc}{$H+}

uses
 {$IFDEF UNIX}{$IFDEF UseCThreads}
 cthreads,
 {$ENDIF}{$ENDIF}
 Interfaces, // this includes the LCL widgetset
 Forms, MainFormU, lazcontrols, tachartlazaruspkg, zcomponent, CompareFormU,
 ExpensesFormU, StrHelper, EditMemoFormU, EditVatScalesFormU, VersionSupport
 { you can add units after this };

{$R *.res}

begin
 RequireDerivedFormResource := True;
 Application.Initialize;
 Application.CreateForm(TMainForm, MainForm);
 Application.CreateForm(TCompareForm, CompareForm);
 Application.CreateForm(TExpensesForm, ExpensesForm);
 Application.CreateForm(TEditMemoForm,EditMemoForm);
 Application.CreateForm(TEditVatScalesForm, EditVatScalesForm);
 Application.Run;
end.

