unit CompareFormU;

{$mode objfpc}{$H+}

interface

uses
 Classes, SysUtils, FileUtil, TAGraph, TASeries, TATools, TAStyles, Forms,
 Controls, Graphics, Dialogs, StdCtrls, Spin;

type

 { TCompareForm }

 TCompareForm = class(TForm)
  btnCompare: TButton;
  lblPension1: TLabel;
  TaxChart: TChart;
  TaxChart1: TLineSeries;
  TaxChart2: TLineSeries;
  IncomeChart1: TLineSeries;
  IncomeChart2: TLineSeries;
  IncomeChart: TChart;
  ChartToolset1: TChartToolset;
  cmbRange1: TComboBox;
  cmbRange2: TComboBox;
  Label1: TLabel;
  Label2: TLabel;
  lblTaxPayed: TLabel;
  lblYourNetIncome: TLabel;
  lblPension: TLabel;
  lblSalary: TLabel;
  lblSalary1: TLabel;
  txtPension1: TFloatSpinEdit;
  txtYears: TSpinEdit;
  txtPension: TFloatSpinEdit;
  txtSalary: TFloatSpinEdit;
  procedure btnCompareClick(Sender: TObject);
  procedure cmbRange1Change(Sender: TObject);
  procedure cmbRange2Change(Sender: TObject);
  procedure FormShow(Sender: TObject);
 private
  { private declarations }
 public
  { public declarations }
 end;

var
 CompareForm: TCompareForm;

implementation

uses MainFormU;

{$R *.lfm}

{ TCompareForm }

procedure TCompareForm.btnCompareClick(Sender: TObject);
var
  Sal1,Sal2,NetSal1,NetSal2,Tax1,Tax2:Double;
  T1,T2,Income1,Income2:Double;
  I: Integer;
begin
 if (cmbRange1.ItemIndex<0) or (cmbRange2.ItemIndex<0) then
 begin
   ShowMessage('You must select both Tax Systems for compare');
   Exit;
 end;


  MainForm.txtPension.Value:=Self.txtPension.Value;
  MainForm.txtSalary.Value:=Self.txtSalary.Value;

  MainForm.cmbRangeSetNames.ItemIndex:=cmbRange1.ItemIndex;
  MainForm.cmbRangeSetNamesChange(nil);

  MainForm.btnCalculate.Click;

  Tax1:=MainForm.txtTax.Value;
  NetSal1 := MainForm.txtNetSalary.Value;


  MainForm.txtPension.Value:=Self.txtPension1.Value;
  MainForm.txtSalary.Value:=Self.txtSalary.Value;

  MainForm.cmbRangeSetNames.ItemIndex:=cmbRange2.ItemIndex;
  MainForm.cmbRangeSetNamesChange(nil);

  MainForm.btnCalculate.Click;

  Tax2:=MainForm.txtTax.Value;
  NetSal2 := MainForm.txtNetSalary.Value;

  TaxChart1.Clear;
  TaxChart2.Clear;
  IncomeChart1.Clear;
  IncomeChart2.Clear;

  T1:=0;
  T2:=0;
  Income1:=0;
  Income2:=0;
  for I:=0 to txtYears.Value - 1 do
  begin
    T1 := T1+Tax1 * 12;
    T2 := T2 +Tax2 * 12;
    Income1:= Income1+ NetSal1 * 12;
    Income2 := Income2 +NetSal2 * 12;

    TaxChart1.AddXY(I,T1);
    TaxChart2.AddXY(I,T2);

    IncomeChart1.AddXY(I,Income1);
    IncomeChart2.AddXY(I,Income2);
  end;

  lblTaxPayed.Caption:= 'Taxes Payed:'+#13#10+' By '+TaxChart1.Title+'='+FloatToStr(T1)+#13#10+' By '+TaxChart2.Title+'='+FloatToStr(T2);

  lblYourNetIncome.Caption:= 'Your Net Income:'#13#10+' By '+TaxChart1.Title+'='+FloatToStr(Income1)+#13#10+' By '+TaxChart2.Title+'='+FloatToStr(Income2);


end;

procedure TCompareForm.cmbRange1Change(Sender: TObject);
begin
 TaxChart1.Title:=cmbRange1.Items[cmbRange1.ItemIndex];
 IncomeChart1.Title:= cmbRange1.Items[cmbRange1.ItemIndex];
end;

procedure TCompareForm.cmbRange2Change(Sender: TObject);
begin
  TaxChart2.Title:=cmbRange2.Items[cmbRange2.ItemIndex];
  IncomeChart2.Title:= cmbRange2.Items[cmbRange2.ItemIndex];
end;

procedure TCompareForm.FormShow(Sender: TObject);
begin
 cmbRange1.Clear;
 cmbRange2.Clear;
 cmbRange1.Items.AddStrings(MainForm.cmbRangeSetNames.Items);
 cmbRange2.Items.AddStrings(MainForm.cmbRangeSetNames.Items);
end;

end.

