unit MainFormU;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqlite3conn, sqldb, DB, FileUtil, DividerBevel, Forms,
  Controls, Graphics, Dialogs, DBCtrls, Buttons, StdCtrls, DBGrids, Spin, Menus,
  ZConnection, ZDataset,types;

type

  { TMainForm }

  TMainForm = class(TForm)
   btnCalculate: TBitBtn;
    btnEditRangeName: TSpeedButton;
    btnAddRangeSet: TSpeedButton;
    cmbRangeSetNames: TComboBox;
    cmbCalcType: TComboBox;
    DividerBevel1: TDividerBevel;
    lblPension: TLabel;
    lblSecondaryConstant: TLabel;
    lblTax1: TLabel;
    btnDeleteRangeSet: TSpeedButton;
    lblSalaryForTax: TLabel;
    MainMenu1: TMainMenu;
    mitCompareExpenses: TMenuItem;
    mitCompareIncomes: TMenuItem;
    mitVatScales: TMenuItem;
    mitsep1: TMenuItem;
    mitAbout: TMenuItem;
    mitHelp: TMenuItem;
    mitCompare: TMenuItem;
    mitExit: TMenuItem;
    mitTools: TMenuItem;
    mitFile: TMenuItem;
    srcRangeList: TDatasource;
    srcRangeSet: TDatasource;
    grdRangeList: TDBGrid;
    DBNavigator1: TDBNavigator;
    grpCalc: TGroupBox;
    grpEditSet: TGroupBox;
    lblNetSalary: TLabel;
    lblRangeList: TLabel;
    lblRangeSet: TLabel;
    lblSalary: TLabel;
    RangeListsID: TLargeintField;
    RangeListsMax: TFloatField;
    RangeListsMin: TFloatField;
    RangeListsPercentage: TFloatField;
    RangeListsRangeSetID: TLargeintField;
    RangeSetsID: TLargeintField;
    RangeSetsName: TMemoField;
    txtSecondaryConstant: TFloatSpinEdit;
    txtSalary: TFloatSpinEdit;
    txtPension: TFloatSpinEdit;
    Con: TZConnection;
    RangeSets: TZTable;
    RangeLists: TZTable;
    qr: TZQuery;
    txtTax: TFloatSpinEdit;
    txtNetSalary: TFloatSpinEdit;
    txtTaxableSalary: TFloatSpinEdit;
    procedure btnAddRangeSetClick(Sender: TObject);
    procedure btnCalculateClick(Sender: TObject);
    procedure btnDeleteRangeSetClick(Sender: TObject);
    procedure btnEditRangeNameClick(Sender: TObject);
    procedure cmbRangeSetChange(Sender: TObject);
    procedure cmbRangeSetNamesChange(Sender: TObject);
    procedure cmbCalcTypeChange(Sender: TObject);
    procedure ConBeforeConnect(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mitAboutClick(Sender: TObject);
    procedure mitCompareClick(Sender: TObject);
    procedure mitCompareExpensesClick(Sender: TObject);
    procedure mitCompareIncomesClick(Sender: TObject);
    procedure mitExitClick(Sender: TObject);
    procedure mitExpensesClick(Sender: TObject);
    procedure mitVatScalesClick(Sender: TObject);
    procedure RangeListsAfterEdit(DataSet: TDataSet);
    procedure RangeListsBeforePost(DataSet: TDataSet);
    procedure RangeSetsAfterPost(DataSet: TDataSet);
    procedure srcRangeSetsDataChange(Sender: TObject; Field: TField);
  private
    { private declarations }
    procedure FillRangeSetNames;
  public
    { public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

uses CompareFormU, ExpensesFormU, EditVatScalesFormU, VersionSupport;

procedure TMainForm.btnAddRangeSetClick(Sender: TObject);
var
  s: string;
begin
  if InputQuery('Make new Range Set', 'Enter RangeSet Name', s) then
  begin
    RangeSets.Insert;
    RangeSetsName.Value := s;
    RangeSets.Post;

    FillRangeSetNames;
    cmbRangeSetChange(nil);
    try
      cmbRangeSetNames.ItemIndex := RangeSets.RecNo;
    except
    end;
  end;
end;

procedure TMainForm.btnCalculateClick(Sender: TObject);
var
  found: boolean;
  SalaryForTaxing: Currency;
  tax: Currency;
begin

  try
    SalaryForTaxing := txtSalary.Value - (txtSalary.Value * txtPension.Value / 100);
    tax := 0;
    case cmbCalcType.ItemIndex of
      0:
      begin
        RangeLists.DisableControls;
        RangeLists.First;
        while not RangeLists.EOF do
        begin

          if ((SalaryForTaxing > RangeListsMin.Value) and
            (SalaryForTaxing < RangeListsMax.Value)) or
            (RangeLists.RecNo = RangeLists.RecordCount) then
          begin
            tax := tax + (SalaryForTaxing - RangeListsMin.Value) *
              RangeListsPercentage.Value / 100;
            found := True;
            Break;
          end
          else
          begin
            tax := tax + (RangeListsMax.Value - RangeListsMin.Value) *
              RangeListsPercentage.Value / 100;
          end;
          RangeLists.Next;
        end;
        txtNetSalary.Value := SalaryForTaxing - tax;
        txtTax.Value := tax;
        txtTaxableSalary.Value := SalaryForTaxing;

      end;

      1:
      begin
        txtTax.Value:=  SalaryForTaxing * txtSecondaryConstant.Value / 100;
        txtNetSalary.Value:= SalaryForTaxing-txtTax.Value;
        txtTaxableSalary.Value:=SalaryForTaxing;
      end;
    end;


  finally
    RangeLists.EnableControls;
  end;

end;

procedure TMainForm.btnDeleteRangeSetClick(Sender: TObject);
begin
  if MessageDlg('Confirmation', 'Are you sure you want to delete selected rangeSet [' +
    RangeSetsName.AsString + '] and all related data?', mtConfirmation,
    mbYesNo, 0) = mrYes then
  begin
    qr.SQL.Text := 'DELETE FROM SalaryRangeLists WHERE SalaryRangeLists.RangeSetID =' +
      FloatToStr(RangeSetsID.Value);
    try
      qr.ExecSQL;
      RangeSets.Delete;
      FillRangeSetNames;
      cmbRangeSetChange(nil);
    except
      on e: Exception do
        ShowMessage(e.Message);
    end;
  end;

end;

procedure TMainForm.btnEditRangeNameClick(Sender: TObject);
var
  s: string;
  I: integer;
begin
  s := RangeSetsName.Value;
  if InputQuery('Edit RangeSet Name', 'Enter New RangeSet Name', s) then
  begin
    RangeSets.Edit;
    RangeSetsName.Value := s;
    RangeSets.Post;
    FillRangeSetNames;
    i := RangeSets.RecNo - 1;
    try
      cmbRangeSetNames.ItemIndex := I;
    except
    end;
  end;
end;

procedure TMainForm.cmbRangeSetChange(Sender: TObject);
begin
  try
    RangeSets.RecNo := cmbRangeSetNames.ItemIndex + 1;
    RangeLists.Filtered := False;
    RangeLists.Filter := 'RangeSetID=' + FloatToStr(RangeSetsID.Value);
    RangeLists.Filtered := True;
  except
  end;

end;

procedure TMainForm.cmbRangeSetNamesChange(Sender: TObject);
begin
  RangeSets.RecNo := cmbRangeSetNames.ItemIndex + 1;
  RangeLists.Filtered := False;
  RangeLists.Filter := 'RangeSetID=' + FloatToStr(RangeSetsID.Value);
  RangeLists.Filtered := True;
end;

procedure TMainForm.cmbCalcTypeChange(Sender: TObject);
begin
 case cmbCalcType.ItemIndex of
   0:
   begin
     lblSecondaryConstant.Caption:='Not Used';
     txtSecondaryConstant.Enabled:=False;
     txtSecondaryConstant.Value:=0;
   end;
   1:
   begin
     lblSecondaryConstant.Caption:='2nd Const %';
     txtSecondaryConstant.Enabled:=True;
     txtSecondaryConstant.Value:=10;
   end;
 end;
end;

procedure TMainForm.ConBeforeConnect(Sender: TObject);
begin
  Con.Database := ExtractFilePath(Application.ExeName) + 'SalaryNetCalc.db';
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
 Caption:=Caption+' v'+VersionSupport.GetFileVersion;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  RangeSets.Open;
  RangeLists.Open;
  FillRangeSetNames;
  cmbRangeSetNames.ItemIndex := 0;
  cmbRangeSetChange(nil);
  cmbCalcTypeChange(nil);

end;

procedure TMainForm.mitAboutClick(Sender: TObject);
begin
 ShowMessage
 ('SalaryNetCalc '+VersionSupport.GetFileVersion + #13#10
 +'======================'+#13#10
 +'Copyrights FU!'+#13#10
 +'======================'+#13#10
 +' flamer0n');
end;

procedure TMainForm.mitCompareClick(Sender: TObject);
begin

end;

procedure TMainForm.mitCompareExpensesClick(Sender: TObject);
begin
  ExpensesForm.ShowModal;
end;

procedure TMainForm.mitCompareIncomesClick(Sender: TObject);
begin
  CompareForm.ShowModal;
end;

procedure TMainForm.mitExitClick(Sender: TObject);
begin
 Application.Terminate;
end;

procedure TMainForm.mitExpensesClick(Sender: TObject);
begin

end;

procedure TMainForm.mitVatScalesClick(Sender: TObject);
begin
 EditVatScalesForm.ShowModal;
end;

procedure TMainForm.RangeListsAfterEdit(DataSet: TDataSet);
begin

end;

procedure TMainForm.RangeListsBeforePost(DataSet: TDataSet);
begin
  RangeListsRangeSetID.Value := RangeSetsID.Value;
end;

procedure TMainForm.RangeSetsAfterPost(DataSet: TDataSet);
begin

end;

procedure TMainForm.srcRangeSetsDataChange(Sender: TObject; Field: TField);
begin

end;

procedure TMainForm.FillRangeSetNames;
begin

  cmbRangeSetNames.Clear;
  RangeSets.First;
  while not RangeSets.EOF do
  begin
    cmbRangeSetNames.Items.Add(RangeSetsName.AsString);
    RangeSets.Next;
  end;

  if (cmbRangeSetNames.Items.Count > 0) then
    cmbRangeSetNames.ItemIndex := 0;

end;

end.
