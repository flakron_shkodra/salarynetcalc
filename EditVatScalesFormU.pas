unit EditVatScalesFormU;

{$mode objfpc}{$H+}

interface

uses
 Classes, SysUtils, db, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
 DBGrids, DbCtrls, MainFormU, ZDataset, Grids;

type

 { TEditVatScalesForm }

 TEditVatScalesForm = class(TForm)
  cmbRange1: TComboBox;
  DBNavigator1: TDBNavigator;
  grd1: TDBGrid;
  lblRangeSet: TLabel;
  lblVat1: TLabel;
  srcVatScales1: TDatasource;
  VatScales1: TZTable;
  VatScales1ID: TLargeintField;
  VatScales1Name: TMemoField;
  VatScales1Percent: TFloatField;
  VatScales1RangeSetID: TLargeintField;
  procedure cmbRange1Change(Sender: TObject);
  procedure FormShow(Sender: TObject);
  procedure grd1CellClick(Column: TColumn);
  procedure grd1DblClick(Sender: TObject);
  procedure grd1PrepareCanvas(sender: TObject; DataCol: Integer;
   Column: TColumn; AState: TGridDrawState);
  procedure VatScales1BeforePost(DataSet: TDataSet);
  procedure VatScales1NameGetText(Sender: TField; var aText: string;
   DisplayText: Boolean);
  procedure VatScales1NameSetText(Sender: TField; const aText: string);
 private
  { private declarations }
 public
  { public declarations }
 end;

var
 EditVatScalesForm: TEditVatScalesForm;

implementation

uses EditMemoFormU;

{$R *.lfm}

{ TEditVatScalesForm }

procedure TEditVatScalesForm.cmbRange1Change(Sender: TObject);
begin

  if not VatScales1.Active then
  VatScales1.Active:=True;

  MainForm.RangeSets.RecNo := cmbRange1.ItemIndex + 1;
  VatScales1.Filtered := False;
  VatScales1.Filter := 'RangeSetID=' + FloatToStr(MainForm.RangeSetsID.Value);
  VatScales1.Filtered := True;
end;

procedure TEditVatScalesForm.FormShow(Sender: TObject);
begin
 cmbRange1.Clear;
 cmbRange1.Items.AddStrings(MainForm.cmbRangeSetNames.Items);
 VatScales1.Close;
end;

procedure TEditVatScalesForm.grd1CellClick(Column: TColumn);
begin
  if grd1.EditorMode then
  if Column.Title.Caption='Name' then
 begin
   VatScales1.Edit;
   EditMemoForm.memEdit.Text:=VatScales1Name.Value;
   if EditMemoForm.ShowModal = mrOK then
   begin
     VatScales1Name.Value:=EditMemoForm.memEdit.Text;
     grd1.SelectedField := VatScales1Percent;
   end;
 end;
end;

procedure TEditVatScalesForm.grd1DblClick(Sender: TObject);
begin

end;

procedure TEditVatScalesForm.grd1PrepareCanvas(sender: TObject;
 DataCol: Integer; Column: TColumn; AState: TGridDrawState);
begin

end;

procedure TEditVatScalesForm.VatScales1BeforePost(DataSet: TDataSet);
begin
  VatScales1RangeSetID.Value := MainForm.RangeSetsID.Value;
end;

procedure TEditVatScalesForm.VatScales1NameGetText(Sender: TField;
 var aText: string; DisplayText: Boolean);
begin
 aText:=Sender.AsString;
 DisplayText:=True;
end;

procedure TEditVatScalesForm.VatScales1NameSetText(Sender: TField;
 const aText: string);
begin
   Sender.Value:=aText;
end;

end.

