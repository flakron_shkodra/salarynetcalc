unit ExpensesFormU;

{$mode objfpc}{$H+}

interface

uses
 Classes, SysUtils, db, FileUtil, DividerBevel, Forms, Controls, Graphics,
 Dialogs, StdCtrls, Spin, Buttons, DBGrids, DbCtrls, ZDataset, MainFormU,
 StrHelper, strutils;

type

 { TExpensesForm }

 TExpensesForm = class(TForm)
  btnAddRangeSet: TSpeedButton;
  btnAddRangeSet1: TSpeedButton;
  btnCalculate: TBitBtn;
  btnDeleteRangeSet: TSpeedButton;
  btnDeleteRangeSet1: TSpeedButton;
  cmbRange1: TComboBox;
  cmbRange2: TComboBox;
  cmbVatScale: TComboBox;
  cmbVatScale1: TComboBox;
  DividerBevel1: TDividerBevel;
  DividerBevel2: TDividerBevel;
  DividerBevel3: TDividerBevel;
  grd1: TDBGrid;
  grd2: TDBGrid;
  grpTax1Expenses: TGroupBox;
  grpTax1Expenses1: TGroupBox;
  Label1: TLabel;
  Label2: TLabel;
  lblExpensePart: TLabel;
  lblExpensePart1: TLabel;
  lblNetSalary1: TLabel;
  lblNetSalary2: TLabel;
  lblTotalVat1: TLabel;
  lblTotalVat2: TLabel;
  lblVat1: TLabel;
  lblVat2: TLabel;
  lblSalary: TLabel;
  lblPension: TLabel;
  lblPension1: TLabel;
  lblVatScales: TLabel;
  lblVatScales1: TLabel;
  lstExpenseParts: TListBox;
  lstExpenseParts1: TListBox;
  qr: TZQuery;
  srcVatScales1: TDatasource;
  srcVatScales2: TDatasource;
  txtExpensePart: TFloatSpinEdit;
  txtExpensePart1: TFloatSpinEdit;
  txtTotalVat1: TFloatSpinEdit;
  txtTotalVat2: TFloatSpinEdit;
  txtPension: TFloatSpinEdit;
  txtPension1: TFloatSpinEdit;
  txtSalary: TFloatSpinEdit;
  VatScales1: TZTable;
  VatScales2: TZTable;
  VatScales1ID: TLargeintField;
  VatScalesID1: TLargeintField;
  VatScales1Name: TMemoField;
  VatScalesName1: TMemoField;
  VatScales1Percent: TFloatField;
  VatScalesPercent1: TFloatField;
  VatScales1RangeSetID: TLargeintField;
  VatScalesRangeSetID1: TLargeintField;
  procedure btnAddRangeSet1Click(Sender: TObject);
  procedure btnAddRangeSetClick(Sender: TObject);
  procedure btnCalculateClick(Sender: TObject);
  procedure btnDeleteRangeSet1Click(Sender: TObject);
  procedure btnDeleteRangeSetClick(Sender: TObject);
  procedure cmbRange1Change(Sender: TObject);
  procedure cmbRange2Change(Sender: TObject);
  procedure FormShow(Sender: TObject);
  procedure grd1DblClick(Sender: TObject);
  procedure grd2DblClick(Sender: TObject);
  procedure VatScales1AfterPost(DataSet: TDataSet);
  procedure VatScales1BeforePost(DataSet: TDataSet);
  procedure VatScales1NameGetText(Sender: TField; var aText: string;
   DisplayText: Boolean);
  procedure VatScales2AfterPost(DataSet: TDataSet);
  procedure VatScales2BeforePost(DataSet: TDataSet);
  procedure VatScalesName1GetText(Sender: TField; var aText: string;
   DisplayText: Boolean);
 private
  { private declarations }
  procedure FillVatScalesCombo;
  procedure FillVatScalesCombo1;
 public
  { public declarations }
 end;

var
 ExpensesForm: TExpensesForm;

implementation

uses EditMemoFormU;

{$R *.lfm}

{ TExpensesForm }

procedure TExpensesForm.cmbRange1Change(Sender: TObject);
begin

  if not VatScales1.Active then
  VatScales1.Active:=True;

  MainForm.RangeSets.RecNo := cmbRange1.ItemIndex + 1;
  VatScales1.Filtered := False;
  VatScales1.Filter := 'RangeSetID=' + FloatToStr(MainForm.RangeSetsID.Value);
  VatScales1.Filtered := True;

  FillVatScalesCombo;


end;

procedure TExpensesForm.btnAddRangeSetClick(Sender: TObject);
var
  itm:string;
begin
  itm :=cmbVatScale.Items[cmbVatScale.ItemIndex]+'='+FloatToStr(txtExpensePart.Value)+'%';
  if lstExpenseParts.Items.IndexOf(itm)<0 then
  lstExpenseParts.Items.Add(itm);
end;

procedure TExpensesForm.btnAddRangeSet1Click(Sender: TObject);
var
  itm:string;
begin
  itm :=cmbVatScale1.Items[cmbVatScale1.ItemIndex]+'='+FloatToStr(txtExpensePart1.Value)+'%';
  if lstExpenseParts1.Items.IndexOf(itm)<0 then
  lstExpenseParts1.Items.Add(itm);
end;

procedure TExpensesForm.btnCalculateClick(Sender: TObject);
var
 NetSal1,NetSal2,Tax1,Tax2:Currency;
 TotalTax1,TotalTax2:Currency;
 I: Integer;
 scaleName:string;
 scalePart:Currency;
 scalePercent:Currency;
 ns:Currency;
begin

txtTotalVat1.Value:=0;
txtTotalVat2.Value:=0;

 if (cmbRange1.ItemIndex<0) or (cmbRange2.ItemIndex<0) then
 begin
   ShowMessage('You must select both Tax Systems ');
   Exit;
 end;

 scalePart:=0;
 for I:= 0 to lstExpenseParts.Count-1 do
 begin
  scalePart:= scalePart+ StrToFloat(StringReplace(SplitString(lstExpenseParts.Items[I],'=')[1],'%','',[rfReplaceAll]));
 end;

 if scalePart>100 then
 begin
   ShowMessage('Your expenses are more than your salary ['+FloatToStr(scalePart)+'%]. Daja prej Zvicrre najsen?');
   Exit;
 end;

 scalePart:=0;

  MainForm.txtPension.Value:=Self.txtPension.Value;
  MainForm.txtSalary.Value:=Self.txtSalary.Value;

  MainForm.cmbRangeSetNames.ItemIndex:=cmbRange1.ItemIndex;
  MainForm.cmbRangeSetNamesChange(nil);

  MainForm.btnCalculate.Click;

  Tax1:=MainForm.txtTax.Value;
  NetSal1 := MainForm.txtNetSalary.Value;


  MainForm.txtPension.Value:=Self.txtPension1.Value;
  MainForm.txtSalary.Value:=Self.txtSalary.Value;

  MainForm.cmbRangeSetNames.ItemIndex:=cmbRange2.ItemIndex;
  MainForm.cmbRangeSetNamesChange(nil);

  MainForm.btnCalculate.Click;

  Tax2:=MainForm.txtTax.Value;
  NetSal2 := MainForm.txtNetSalary.Value;

  VatScales1.First;

  TotalTax1:=Tax1;
  TotalTax2:=Tax2;

  lblNetSalary1.Caption:='Net='+FloatToStr(NetSal1);
  lblNetSalary2.Caption:='Net='+FloatToStr(NetSal2);
  for I:=0 to lstExpenseParts.Count-1 do
  begin
    scaleName:= SplitString(lstExpenseParts.Items[I],'=')[0];
    scalePart:= StrToFloat(StringReplace(SplitString(lstExpenseParts.Items[I],'=')[1],'%','',[rfReplaceAll]));
    VatScales1.First;
    VatScales1.Locate('Name',scaleName,[loCaseInsensitive]);
    scalePercent:= VatScales1Percent.Value;
    ns :=  (NetSal1 * scalePart/100);
    ns := (ns * scalePercent / 100);
    TotalTax1:=TotalTax1+ns;
  end;


  for I:= 0 to lstExpenseParts1.Count-1 do
  begin
    scaleName:= SplitString(lstExpenseParts1.Items[I],'=')[0];
    scalePart:= StrToFloat(StringReplace(SplitString(lstExpenseParts1.Items[I],'=')[1],'%','',[rfReplaceAll]));
    VatScales2.First;
    VatScales2.Locate('Name',scaleName,[loCaseInsensitive]);
    scalePercent:= VatScalesPercent1.Value;
    ns :=  (NetSal1 * scalePart/100);
    ns := (ns * scalePercent / 100);
    TotalTax2:=TotalTax2+ns;
  end;

  txtTotalVat1.Value:=TotalTax1;
  txtTotalVat2.Value:=TotalTax2;

end;

procedure TExpensesForm.btnDeleteRangeSet1Click(Sender: TObject);
begin
 if lstExpenseParts1.ItemIndex>-1 then
 lstExpenseParts1.Items.Delete(lstExpenseParts1.ItemIndex);
end;

procedure TExpensesForm.btnDeleteRangeSetClick(Sender: TObject);
begin
 if lstExpenseParts.ItemIndex>-1 then
 lstExpenseParts.Items.Delete(lstExpenseParts.ItemIndex);
end;

procedure TExpensesForm.cmbRange2Change(Sender: TObject);
begin

  if not VatScales2.Active then
  VatScales2.Active:=True;

  MainForm.RangeSets.RecNo := cmbRange2.ItemIndex + 1;
  VatScales2.Filtered := False;
  VatScales2.Filter := 'RangeSetID=' + FloatToStr(MainForm.RangeSetsID.Value);
  VatScales2.Filtered := True;

   FillVatScalesCombo1;
end;

procedure TExpensesForm.FormShow(Sender: TObject);
begin

 VatScales1.Close;
 VatScales2.Close;

 cmbRange1.Clear;
 cmbRange2.Clear;
 cmbRange1.Items.AddStrings(MainForm.cmbRangeSetNames.Items);
 cmbRange2.Items.AddStrings(MainForm.cmbRangeSetNames.Items);

  cmbVatScale.Clear;
  cmbVatScale1.Clear;

 lblNetSalary2.Caption:='';
 lblNetSalary1.Caption:='';




end;

procedure TExpensesForm.grd1DblClick(Sender: TObject);
begin
 EditMemoForm.memEdit.Text:=VatScales1Name.AsString;
 if EditMemoForm.ShowModal=mrOK then
 begin
   VatScales1Name.Value:=EditMemoForm.memEdit.Text;
 end;
end;

procedure TExpensesForm.grd2DblClick(Sender: TObject);
begin
 EditMemoForm.memEdit.Text:=VatScales1Name.AsString;
 if EditMemoForm.ShowModal=mrOK then
 begin
   VatScalesName1.Value:=EditMemoForm.memEdit.Text;
 end;
end;

procedure TExpensesForm.VatScales1AfterPost(DataSet: TDataSet);
begin
 FillVatScalesCombo;
end;

procedure TExpensesForm.VatScales1BeforePost(DataSet: TDataSet);
begin
 VatScales1RangeSetID.Value := MainForm.RangeSetsID.Value;
end;

procedure TExpensesForm.VatScales1NameGetText(Sender: TField;
 var aText: string; DisplayText: Boolean);
begin
 aText:=Sender.AsString;
 DisplayText:=True;
end;

procedure TExpensesForm.VatScales2AfterPost(DataSet: TDataSet);
begin
 FillVatScalesCombo;
end;

procedure TExpensesForm.VatScales2BeforePost(DataSet: TDataSet);
begin
 VatScalesRangeSetID1.Value := MainForm.RangeSetsID.Value;
end;

procedure TExpensesForm.VatScalesName1GetText(Sender: TField;
 var aText: string; DisplayText: Boolean);
begin
 aText:=Sender.AsString;
 DisplayText:=True;
end;

procedure TExpensesForm.FillVatScalesCombo;
begin
 cmbVatScale.Items.Clear;
 VatScales1.First;
 while not VatScales1.EOF do
 begin
  cmbVatScale.Items.Add(VatScales1Name.AsString);
  VatScales1.Next;
 end;
end;

procedure TExpensesForm.FillVatScalesCombo1;
begin
 cmbVatScale1.Items.Clear;
 VatScales2.First;
 while not VatScales2.EOF do
 begin
  cmbVatScale1.Items.Add(VatScalesName1.AsString);
  VatScales2.Next;
 end;
end;

end.

